import {Component} from '@angular/core';

@Component({
  selector: 'app-numbers',
  templateUrl: './numbers.component.html',
  styleUrls: ['./numbers.component.css']
})

export class NumbersComponent {
  numbers: number[] = [];

  constructor() {
    this.numbers = this.generateNumbers();
  }

  generateNumbers() {
    const testArray: number[] = [];
    while(testArray.length < 5) {
      let randomNumber = Math.floor(Math.random() * (37 - 5)) + 5;
      if(testArray.indexOf(randomNumber) === -1) {
        testArray.push(randomNumber);
      }
    }
    testArray.sort((a, b) => {
      return a - b;
    });
    return testArray;
  }

  generateNewNumbers () {
    this.numbers = this.generateNumbers();
  }
}
